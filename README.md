# OpenML dataset: Waterstress

https://www.openml.org/d/42464

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Ankita Gupta, Dr.Lakwinder Kaur, Dr. Gurmeet Kaur    
**Source**: Unknown - 01-11-2019 
**Please cite**:   

Water stress dataset for Indian variety of wheat crop: 

The data consist of a file system-based data of Raj 3765 variety of wheat. There are twenty-four chlorophyll fluorescence images captured every alternative day (Control and Drought) that have been captured for a period of sixty days. A total of (594 x 2) images are used for this research work.
This dataset comprises of images of wheat crop using Chlorophyll Fluorescence modality. Which is further used to identify drought water stress at canopy level in the wheat crop with the help of  Image Processing algorithms.



Autocorrelation: (out.autoc)
Contrast: matlab (out.contr)
Correlation: matlab (out.corrm)
4.Correlation: (out.corrp)
5.Cluster Prominence: (out.cprom)
Cluster Shade: (out.cshad)
7.Dissimilarity: (out.dissi)
Energy: matlab (out.energ)
Entropy: (out.entro)
Homogeneity: matlab (out.homom)
Homogeneity: (out.homop)
Maximum probability: (out.maxpr)
Sum of sqaures: Variance (out.sosvh)
Sum average (out.savgh)
Sum variance (out.svarh)
Sum entropy (out.senth)
Difference variance (out.dvarh)
Difference entropy (out.denth)
Information measure of correlation1 (out.inf1h)
Informaiton measure of correlation2 (out.inf2h)
Inverse difference (INV) is homom (out.homom)
Inverse difference normalized (INN) (out.indnc)
Inverse difference moment normalized (out.idmnc)
These variables then undergone through various statistical processes to identify the key detection variables suited best for water stress which in-turn help to build root cause analysis model (RCA) for water stress.
The dataset has been produced using MATLAB GLCM libraries https://in.mathworks.com/help/images/ref/graycomatrix.html

Texture feature analysis is done using 23 texture GLCM features to extract features pertaining to water stress identification.


These variables then undergone through various statistical processes to identify the key detection variables suited best for water stress which in-turn help to build root cause analysis model (RCA) for water stress.
The dataset has been produced using MATLAB GLCM libraries https://in.mathworks.com/help/images/ref/graycomatrix.html

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42464) of an [OpenML dataset](https://www.openml.org/d/42464). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42464/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42464/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42464/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

